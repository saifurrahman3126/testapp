//
//  BannerCollectionViewCell.swift
//  TestApp
//
//  Created by Saifur Rahman on 17/04/22.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
