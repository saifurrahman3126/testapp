//
//  ItemsTableViewCell.swift
//  TestApp
//
//  Created by Saifur Rahman on 17/04/22.
//

import UIKit
import Cosmos
class ItemsTableViewCell: UITableViewCell {
    var c = CosmosSettings()
    var data : ItemModal! = nil
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        
    }
    func refreshView(){
        if let _ = data{
            logoImg.image = data.icon
            title.text = data.title
            desc.text = data.description
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
