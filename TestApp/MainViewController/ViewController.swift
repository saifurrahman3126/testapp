//
//  ViewController.swift
//  TestApp
//
//  Created by Saifur Rahman on 16/04/22.
//

import UIKit
import UIKit

struct ItemModal {
    var icon: UIImage
    var title: String
    var description: String
}
class ViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var itemsTable: UITableView!
    @IBOutlet weak var pager: UIPageControl!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    var bannerData = [(#imageLiteral(resourceName: "check-out-pic"),"Lorem?" , "Hi , I am ipsum"),(#imageLiteral(resourceName: "bg5"),"Lorem?" , "Hi , I am ipsum"),(#imageLiteral(resourceName: "bg1"),"Lorem?" , "Hi , I am ipsum")]
    var menu = [ItemModal]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fillData()
        itemsTable.delegate = self
        itemsTable.dataSource = self
        itemsTable.tableViewSetupWithViewC(ViewController: self, cellArrayToRegister: [ItemsTableViewCell.getCellIdentifier()])
        
        bannerCollectionView.collectionViewSetupWithViewC(ViewController: self, cellArrayToRegister: [BannerCollectionViewCell.getCellIdentifier()])
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        let layoutCollection = UICollectionViewFlowLayout()
            layoutCollection.minimumInteritemSpacing = 0
            layoutCollection.minimumLineSpacing = 0
        layoutCollection.scrollDirection = .horizontal
            bannerCollectionView.collectionViewLayout = layoutCollection
        bannerCollectionView.isPagingEnabled = true
        // Do any additional setup after loading the view.
    }
    func fillData(){
         menu = [
            ItemModal(icon: #imageLiteral(resourceName: "bg3"), title: "Le Bernardian", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard"),
            ItemModal(icon: #imageLiteral(resourceName: "r2"), title: "The Groove", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard"),
            ItemModal(icon: #imageLiteral(resourceName: "r3"), title: "Lesros Thai", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard"),
            ItemModal(icon: #imageLiteral(resourceName: "r4"), title: "Gyro King", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard"),
            ItemModal(icon: #imageLiteral(resourceName: "bg1"), title: "Samovar Tea Lounge", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard"),
            ItemModal(icon: #imageLiteral(resourceName: "bg3"), title: "Ipsy Cafe", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard"),
            ItemModal(icon: #imageLiteral(resourceName: "r5"), title: "Handy Stall", description: "Lorem Ipsum is simply dummy text of printing and typesetting industry. orem ipsum has been the industry's standard")
        ]
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

           let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
           pager.currentPage = Int(pageNumber)
       }
}
extension ViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsTableViewCell") as! ItemsTableViewCell
        cell.data = menu[indexPath.row]
        cell.refreshView()
        return cell
    }
}

extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        cell.bannerImage.image = bannerData[indexPath.row].0
        cell.heading.text = bannerData[indexPath.row].1
        cell.descriptionLbl.text = bannerData[indexPath.row].2
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: collectionView.frame.height)
    }
}
